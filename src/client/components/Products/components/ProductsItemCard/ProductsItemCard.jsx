import React from 'react';
import Col from "react-bootstrap/cjs/Col";
import Button from 'react-bootstrap/Button';
import './ProductsItemCard.css'
import { addItem } from "../../../../../store/actionCreators";
import { useDispatch } from "react-redux";

const ProductsItemCard = (product) => {
  const {id, title, image, price} = product;
  const dispatch = useDispatch();

  return (
      <Col key={id} className="col">
        <div className="card">
          <img className="card-img-top" src={image} alt="Card img" />
          <div className="card-block">
            <h4 className="card-title">{title}</h4>
            <p className="card-text">Price: ${price}</p>
            <Button type="button" variant="primary" onClick={() => dispatch(addItem(id))}>Add to Cart</Button>
          </div>
        </div>
      </Col>
  );
};

export default ProductsItemCard;
