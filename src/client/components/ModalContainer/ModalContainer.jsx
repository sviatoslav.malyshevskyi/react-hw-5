import React from 'react';
import { useDispatch, useSelector } from "react-redux";
import { modalClose } from "../../../store/actionCreators";
import Button from 'react-bootstrap/Button';
import './ModalContainer.css';

const ModalContainer = ({title, children}) => {
  const modalOpen = useSelector(state => state.modalOpen);
  const verifyOpenState = modalOpen ? 'modal modal-open' : 'modal';
  const dispatch = useDispatch();

    return (
      <div className={verifyOpenState}>
        <div className="modal-lg">
          <div className="modal-content">
            <div className="modal-header flex a-j-center-between pl10prc">
              <h5 className="modal-title">{title}</h5>
              <Button variant="primary" className="close btn-sm" onClick={() => dispatch(modalClose())}><span> x </span></Button>
            </div>
            <div className="modal-body">{children}</div>
            <div className="modal-footer">
              <Button variable="danger" onClick={() => dispatch(modalClose())} className="btn-right btn-danger">Close</Button>
            </div>
          </div>
        </div>
      </div>
    );
  };

export default ModalContainer;
