import React from 'react';
import Button from 'react-bootstrap/Button';
import './Navbar.css';
import { clearCart, modalOpen } from "../../../store/actionCreators";
import { useDispatch, useSelector } from "react-redux";

const Navbar = () => {
    const shoppingCart = useSelector(state => state.shoppingCart);
    const totalCount = shoppingCart.reduce((sum, item) => sum + item.count, 0);
    const dispatch = useDispatch();

    return (
        <nav className="navbar navbar-inverse bg-inverse fixed-top bg-faded">
            <div className="row nav-row">
                <div className="col">
                    <Button variant="primary" onClick={() => dispatch(modalOpen())}
                            data-toggle="modal" data-target="#cart">
                        Cart (<span className="total-count">{totalCount}</span>)
                    </Button>{' '}
                    <Button variant="primary" onClick={() => dispatch(clearCart())}>Clear Cart</Button>
                </div>
            </div>
        </nav>
    );
};

export default Navbar;
