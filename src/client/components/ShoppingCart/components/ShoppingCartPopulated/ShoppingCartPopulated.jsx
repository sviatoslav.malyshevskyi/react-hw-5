import React from 'react';
import './ShoppingCartPopulated.css';
import ShoppingCartItem from '../ShoppingCartItem';
import { useSelector } from "react-redux";

const ShoppingCartPopulated = () => {
  const shoppingCart = useSelector(state => state.shoppingCart);
  const className = (shoppingCart.length > 0) ? 'shopping-cart show' : 'shopping-cart'
  const priceTotal = Number(shoppingCart.reduce((sum, item) => sum + item.total, 0).toFixed(2));
  const shoppingCartItems = shoppingCart.map(item =>
      <ShoppingCartItem key={item.id} {...item} /> );

  return (
      <div className={className}>
        <table className="show-cart table a-j-center-between">
          <thead>
          <tr>
            <th>Item</th>
            <th>Price</th>
            <th>Quantity</th>
            <th>Total</th>
            <th>Remove?</th>
          </tr>
          </thead>
          <tbody>{shoppingCartItems}</tbody>
        </table>
        <hr/>
        <div className="txt-bold pl7prc ">Total: $<span className="total-cart">{priceTotal}</span></div>
      </div>
  );
};

export default ShoppingCartPopulated;
