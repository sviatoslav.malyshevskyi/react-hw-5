import React from 'react';
import './ShoppingCartEmpty.css'
import { useSelector } from "react-redux";

const ShoppingCartEmpty = () => {
  const shoppingCart = useSelector(state => state.shoppingCart);
  const displayMessage = (shoppingCart.length > 0) ? 'empty-cart' : 'empty-cart show'

  return (
      <h6 className={displayMessage}><span className="red txt-lg">Your cart is empty!</span></h6>
  );
};

export default ShoppingCartEmpty;
