import { ADD_ITEM, REMOVE_ITEM, DELETE_ITEM, CLEAR_CART, UPDATE_CART, MODAL_OPEN, MODAL_CLOSE } from "./constants";

export const addItem = (payload) => {
  return{type: ADD_ITEM, payload};
};

export const removeItem = (payload) => {
  return {type: REMOVE_ITEM, payload};
};

export const deleteItem = (payload) => {
  return {type: DELETE_ITEM, payload};
};

export const clearCart = (payload) => {
  return {type: CLEAR_CART, payload};
};

export const updateCart = (value, index) => {
  return {type: UPDATE_CART, payload: {value, index}};
};

export const modalOpen = (payload) => {
  return {type: MODAL_OPEN, payload};
};

export const modalClose = (payload) => {
  return {type: MODAL_CLOSE, payload};
};
